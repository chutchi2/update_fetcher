# You can override the included template(s) by including variable overrides
# SAST customization: https://docs.gitlab.com/ee/user/application_security/sast/#customizing-the-sast-settings
# Secret Detection customization: https://docs.gitlab.com/ee/user/application_security/secret_detection/#customizing-settings
# Dependency Scanning customization: https://docs.gitlab.com/ee/user/application_security/dependency_scanning/#customizing-the-dependency-scanning-settings
# Container Scanning customization: https://docs.gitlab.com/ee/user/application_security/container_scanning/#customizing-the-container-scanning-settings
# Note that environment variables can be set in several places
# See https://docs.gitlab.com/ee/ci/variables/#cicd-variable-precedence
image: mcr.microsoft.com/dotnet/sdk:6.0
include:
- template: Security/SAST.gitlab-ci.yml

stages:
  - test
  - build
  - deploy

sast:
  stage: test

variables:
  OBJECTS_DIRECTORY: 'obj'
  NUGET_PACKAGES_DIRECTORY: '.nuget'
  SOURCE_CODE_PATH: '*/*/'
  VERSION_INFO: "beta-${CI_COMMIT_REF_NAME}"

workflow:
  rules:
    - if: $CI_COMMIT_TAG
      variables:
        VERSION_INFO: "Release"
        RELEASE_NAME: $CI_COMMIT_REF_NAME
    - when: always
  

cache:
  key: "$CI_JOB_STAGE-$CI_COMMIT_REF_SLUG"
  paths:
    - '$SOURCE_CODE_PATH$OBJECTS_DIRECTORY/project.assets.json'
    - '$SOURCE_CODE_PATH$OBJECTS_DIRECTORY/*.csproj.nuget.*'
    - '$NUGET_PACKAGES_DIRECTORY'
  policy: pull-push

before_script:
  - 'dotnet restore --packages $NUGET_PACKAGES_DIRECTORY'

build:
  stage: build
  script:
    - 'dotnet build --no-restore'
  artifacts:
    paths: ['bin/*']

.publish:
  stage: build
  script:
    - git fetch --all --tags
    - export LATEST_VER=$(git describe)
    - echo dotnet publish -r ${ARCH} ${ARGS} -p:AssemblyVersion=${LATEST_VER} -p:Version=${LATEST_VER}-${VERSION_INFO}
    - dotnet publish -r ${ARCH} ${ARGS} -p:AssemblyVersion=${LATEST_VER} -p:Version=${LATEST_VER}-${VERSION_INFO}
  artifacts:
    name: artifacts-${ARCH}
    expire_in: never
    paths: ['bin/**/publish/**']

publish_linux_x64:
  extends: .publish
  variables: 
    ARCH: "linux-x64"
    ARGS: "-p:PublishSingleFile=true -c Release --self-contained true -p:EnableCompressionInSingleFile=true"
    
publish_win_x64:
  extends: .publish
  variables:
    ARCH: "win-x64"
    ARGS: "-p:PublishSingleFile=true -c Release --self-contained true -p:EnableCompressionInSingleFile=true"

publish_osx_x64:
  extends: .publish
  variables:
    ARCH: "osx-x64"
    ARGS: "-c Release --self-contained true"
  
publish_osx_arm64:
  extends: .publish
  variables:
    ARCH: "osx-arm64"
    ARGS: "-c Release --self-contained true"
    
release_job:
  stage: deploy
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  needs: ["publish_linux_x64", "publish_osx_x64", "publish_osx_arm64", "publish_win_x64"]
  rules:
    - if: $CI_COMMIT_TAG                  # Run this job when a tag is created manually
  before_script:
    - echo "running release_job for ${CI_COMMIT_TAG}"
  script:
    - ls -laR ./
    - mkdir -p UpdateFetcher_linux-x64
    - cp ./bin/Release/net6.0/linux-x64/publish/UpdateFetcher ./UpdateFetcher_linux-x64/
    - mkdir -p UpdateFetcher_osx-x64 
    - cp ./bin/Release/net6.0/osx-x64/publish/UpdateFetcher ./UpdateFetcher_osx-x64/
    - mkdir -p UpdateFetcher_osx-arm64
    - cp ./bin/Release/net6.0/osx-arm64/publish/UpdateFetcher ./UpdateFetcher_osx-arm64/
    - mkdir -p UpdateFetcher_win-x64
    - cp ./bin/Release/net6.0/win-x64/publish/UpdateFetcher.exe ./UpdateFetcher_win-x64/
    - tar -czvf UpdateFetcher_linux-x64.tar.gz ./UpdateFetcher_linux-x64
    - tar -czvf UpdateFetcher_osx-x64.tar.gz ./UpdateFetcher_osx-x64
    - tar -czvf UpdateFetcher_osx-arm64.tar.gz ./UpdateFetcher_osx-arm64
    - tar -czvf UpdateFetcher_win-x64.tar.gz ./UpdateFetcher_win-x64
  artifacts:
    expire_in: never
    paths: ['UpdateFetcher_linux-x64.tar.gz', 'UpdateFetcher_osx-x64.tar.gz', 'UpdateFetcher_osx-arm64.tar.gz', 'UpdateFetcher_win-x64.tar.gz']
  release:
    name: 'Release $CI_COMMIT_TAG'
    description: 'Created using the release-cli $EXTRA_DESCRIPTION'  # $EXTRA_DESCRIPTION must be defined
    tag_name: '$CI_COMMIT_TAG'                                       # elsewhere in the pipeline.
    ref: '$CI_COMMIT_TAG'
    assets: # Optional, multiple asset links
      links:
        - name: 'UpdateFetcher_linux-x64.tar.gz'
          url: 'https://gitlab.com/g4933/gen5w/update_fetcher/-/jobs/${CI_JOB_ID}/artifacts/raw/UpdateFetcher_linux-x64.tar.gz'
        - name: 'UpdateFetcher_osx-x64.tar.gz'
          url: 'https://gitlab.com/g4933/gen5w/update_fetcher/-/jobs/${CI_JOB_ID}/artifacts/raw/UpdateFetcher_osx-x64.tar.gz'
        - name: 'UpdateFetcher_osx-arm64.tar.gz'
          url: 'https://gitlab.com/g4933/gen5w/update_fetcher/-/jobs/${CI_JOB_ID}/artifacts/raw/UpdateFetcher_osx-arm64.tar.gz'
        - name: 'UpdateFetcher_win-x64.tar.gz'
          url: 'https://gitlab.com/g4933/gen5w/update_fetcher/-/jobs/${CI_JOB_ID}/artifacts/raw/UpdateFetcher_win-x64.tar.gz'