using System;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using IniParser;
using IniParser.Model;

namespace UpdateFetcher;

public static class IniParserExtension
{
    public static async Task<IniData> ReadLgeInfAsync(this FileIniDataParser parser, string infPath)
    {
        var inf = new StringBuilder();
        foreach (var line in await File.ReadAllLinesAsync(infPath))
        {
            var x = line.Replace("[FILE_", $"[{Guid.NewGuid()}_FILE_").Replace("\\","/");  
            inf.AppendLine(x);
        }
        return parser.Parser.Parse(inf.ToString());
    }
}