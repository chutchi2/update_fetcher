using System.CommandLine;
// using System.IO.Compression;
using UpdateFetcher.Regions;
using static UpdateFetcher.Downloader;

namespace UpdateFetcher;

public static class Program
{
    private static IRegion Region { get; set; } = null!;
    private static string Model { get; set; } = "2020_21_Ioniq_Plug_in_Hybrid_EU";


    private static async Task<int> Main(string[] args)
    {
        var rootCommand = new RootCommand("Update fetcher for hyundai / kia");
        var regionOption = new Option<SupportedRegion>(
            name: "--region",
            description: "The region to grab the info from.")
            { IsRequired = true };

        var modelOption = new Option<string>(
            name: "--model",
            description: "Car model to fetch.")
            { IsRequired = true };

        var infOption = new Option<string>(
                name: "--inf",
                description: "Path to inf file.")
            { IsRequired = true };

        var destOption = new Option<string>(
            getDefaultValue: () => "./downloads/updates/",
            name: "--dest",
            description: "Destination path to save the files.");

        var infCommand = new Command("inf", "Fetches all the infs available for the chosen model.")
        {
            regionOption,
            modelOption
        };
        infCommand.SetHandler(async (region, model) => await FindInfFiles(region, model), regionOption, modelOption);
        rootCommand.AddCommand(infCommand);

        var downloadCommand = new Command("download", "Downloads a chosen update.")
        {
            infOption,
            destOption
        };

        downloadCommand.SetHandler(async (inf, dest) => await Download(inf, dest), infOption, destOption);
        rootCommand.AddCommand(downloadCommand);

        var listCommand = new Command("list", "Lists known model names.")
        {
            regionOption
        };

        listCommand.SetHandler(ListModels, regionOption);
        rootCommand.AddCommand(listCommand);

        return await rootCommand.InvokeAsync(args);
    }

    private static void ListModels(SupportedRegion requestedRegion)
    {
        var region = GetRegion(requestedRegion);
        region.KnownModelNames.ToList().ForEach(Console.WriteLine);
    }

    private static async Task Download(string infFile, string dest)
    {
        await DownloadUpdate(infFile, dest);
    }


    private static async Task FindInfFiles(SupportedRegion requestedRegion, string requestedModel)
    {
        var region = GetRegion(requestedRegion);
        await FindInfFiles(region, requestedModel);
    }

    private static IRegion GetRegion(SupportedRegion requestedRegion) => new Region(requestedRegion);

    private static async Task FindInfFiles(IRegion region, string requestedModel)
    {
        var model = region.KnownModelNames.SingleOrDefault(known => known.Contains(requestedModel));
        if (model == null)
        {
            Console.WriteLine($"Warning: [{requestedModel}] is not on the list of known models for [{region.Code}]. We will still try it.");
            model = requestedModel;
        }

        
        var years = new List<int>(Enumerable.Range(18, 6));
        var months = new List<int>(Enumerable.Range(1, 12));
        var revs = new List<char>(Enumerable.Range('A', 26).Select(x => (char)x));

        var possibleVersions =
            (from year in years
                from month in months
                from rev in revs
                select $"{(region.LongDateFormat ? "20" : "")}{year}{month.ToString("D2")}{rev}")
            .ToArray();

        await Task.WhenAll(possibleVersions.Select(async v =>
        {
            var uri = new Uri(region.DownloadUri, $"{v}/{model}.inf");
            var httpContent = await DownloadFileAsync(uri);

            if (httpContent != null)
            {
                var fileName = $"downloads/{requestedModel}/{v}_{model}.inf";
                Directory.CreateDirectory(Path.GetDirectoryName(fileName) ?? throw new InvalidOperationException());
                await using var fs = new FileStream(fileName, FileMode.OpenOrCreate, FileAccess.ReadWrite,
                    FileShare.ReadWrite);
                fs.SetLength(0);
                await httpContent.CopyToAsync(fs);
                if (httpContent.Headers.LastModified != null)
                    File.SetLastWriteTime(fileName, httpContent.Headers.LastModified.Value.DateTime);
                Console.WriteLine($"Fetched from {uri} | Stored at {fileName}");
            }
        }));
    }
}