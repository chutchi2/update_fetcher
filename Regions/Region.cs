using System.Text;
using System.Text.Json;
using static UpdateFetcher.Downloader;

namespace UpdateFetcher.Regions;

public sealed class Region : IRegion
{
    public SupportedRegion RegionEnum { get; }
    private string Name { get; }
    public string Code => RegionEnum == SupportedRegion.Au ? "NA" : Name;

    public bool LongDateFormat => RegionEnum == SupportedRegion.Kr;

    public Uri DownloadUri =>
        RegionEnum == SupportedRegion.Kr
            ? new Uri("http://avn01.speednavi.co.kr/avn/SmartUpdate/INF_Data/")
            : new Uri($"http://oem-{Name}upload.map-care.com/{Code.ToUpper()}_update_data/{Code.ToUpper()}_INF/");

    internal Region(SupportedRegion regionEnum)
    {
        RegionEnum = regionEnum;
        Name = regionEnum.ToString();
    }

    public IEnumerable<string> KnownModelNames
    {
        get
        {
            var models = new List<string>();
            Task.Run(async () =>
            {
                var token = await GetToken();
                var hmModels = FindModelNames(true, token);
                var kmModels = FindModelNames(false, token);
                models.AddRange(await hmModels);
                models.AddRange(await kmModels);
            }).Wait();
            return models;
        }
    }

    private async Task<string> GetToken()
    {
        var uri = new Uri("https://apieu.map-care.com/api/GetGUIDV2");
        var body = new
        {
            D1 = Name
        };

        var data = new StringContent(JsonSerializer.Serialize(body), Encoding.UTF8, "text/plain");
        var response = await SendPostAsync(uri, data);
        return response != null ? (await response.ReadAsStringAsync()).Split("|").Last().TrimEnd() : "";
    }

    public async Task<IEnumerable<string>> FindModelNames(bool isHm = true, string? token = null)
    {
        var uri = new Uri("https://apieu.map-care.com/api/ChkRegistV2");
        var body = new
        {
            D1 = "",
            D2 = isHm ? "HM" : "KM",
            D3 = "",
            D4 = Name,
            D5 = token ?? await GetToken(),
            D6 = "U"
        };

        // Console.WriteLine(JsonSerializer.Serialize(body)); // debug

        var data = new StringContent(JsonSerializer.Serialize(body), Encoding.UTF8, "text/plain");
        var response = await SendPostAsync(uri, data);

        if (response == null)
            return Array.Empty<string>();

        return (await response.ReadAsStringAsync())
            .Split("\n")
            .Where(line => line.Contains("$1|"))
            .SelectMany(col => col.Split("|").Where(record => record.Contains(".inf")))
            .Select(model => model.Replace($".inf$1", ""));
    }
}